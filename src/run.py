from src.app import app
from src.common.config import DEBUG

app.run(debug=DEBUG)
